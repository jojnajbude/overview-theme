const featureProduct = document.querySelector('.feature-product--js');
const featureProductSection = document.querySelector('.feature-product');
const points = featureProduct.querySelectorAll('.point');
const hints = featureProduct.querySelectorAll('.hint');

const svg = featureProduct.querySelector('.feature-product__connect-lines');

const pointBlocks = [];

for (const point of points) {
  const color = '#ef392f';
  const strokeWidth = 1.5;

  const id = point.dataset.connect;
  const hint = Array.from(hints).find(finded => finded.dataset.connect === id);

  const lineCoords = {
    start: {
      x: point.offsetLeft + (point.offsetWidth / 2),
      y: point.offsetTop,
    },
    end: {
      x: hint.offsetLeft,
      y: hint.offsetTop + (hint.offsetHeight / 2),
    }
  }

  const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
  line.setAttribute('x1', lineCoords.start.x);
  line.setAttribute('y1', lineCoords.start.y);
  line.setAttribute('x2', lineCoords.end.x);
  line.setAttribute('y2', lineCoords.end.y);
  line.setAttribute('stroke', color);
  line.setAttribute('stroke-width', strokeWidth);
  line.classList.add(`opacity-delay--${id}`);
  line.setAttribute('data-connect', id);

  svg.appendChild(line);


  const pointBlock = {point, hint, line};
  pointBlocks.push(pointBlock);
}

const lines = svg.querySelectorAll('line');

function isInViewport(element) {
  const rect = element.getBoundingClientRect();
  return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

const hintOnView = (point) => {
  const pointScroll = featureProductSection.offsetTop - 500 + (75 * (+point.dataset.connect + 1));
  const pointEndScroll = pointScroll + featureProduct.offsetHeight;

  const visible = pointScroll <= window.scrollY
    && pointEndScroll >= window.scrollY;

  return visible;
};

const featureOnView = () => {
  for (const block of pointBlocks) {
    const opacityValue = hintOnView(block.point) ? 1 : 0;

    for (const elem in block) {
      block[elem].style.opacity = opacityValue;
    }
  }
};

featureOnView();
window.addEventListener('scroll', () => {
  featureOnView();
});
